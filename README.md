To clone this repository onto the BeagleBoneBlack (BBB):
1- Ensure that the BBB by following instructions in 'README-BBB Set up' under SOF-0002. 
2- use git clone command to clone the repository onto BBB's Desktop. 

The following are the instructions to set up a new BBB

------------------------------------
Setting up a new BeagleBoneBlack:
1-	Power up the new BeagleBoneBlack (BBB) and connect it to a monitor screen, mouse and a keyboard.
2-	Once turned on, navigate to QTerminal (Main Menu->Systems Tools->QTerminal).
3-	On QTerminal, type cat /etc/dogtag or cat /etc/debian_version if the first command does not work. We want the Debian version 8.10. Any older version is not compatible to run the python PCR programs. 
4-	On QTerminal, type uname -a or uname -r. We want to kernel version to be 4.4. Any older or newer version will not be compatible to run the software programs. 
5-	If one or both versions are not desired, the new Debian 8.10 Operating system needs to be flashed onto the BBB. 
6-	Type command sudo nano /boot/uEnv.txt, which will open a text file.
7-	In the text file that is now open, change the lines:
a.	##enable BBB: eMMC Flasher:
	#cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh 

		TO 
		##enable BBB: eMMC Flasher:
	    cmdline=init=/opt/scripts/tools/eMMC/init-eMMC-flasher-v3.sh 
b.	Save the file and return to command line by entering/typing:
i.	Press ‘Ctrl’ and then ‘X’
ii.	The prompt will ask whether you want to save your changes: Press ‘Y’
iii.	The prompt will ask to confirm the file name: Press ‘Enter’
8-	Shutdown the BBB by typing in the command sudo shutdown now on QTerminal.
9-	Download Debian 8.10 image on your computer from here: https://beagleboard.org/latest-images. The image is also in SOF-0002. 
10-	Follow the steps (http://beagleboard.org/getting-started) to write the Debian 8.10 image to the microSD card:
a.	Download and install Etcher onto your computer.
b.	Use your computer's SD slot or a USB adapter to connect the SD card to your computer.
c.	Use Etcher to write the Debian 8.10 image on your microSD card. Etcher will automatically decompress the image before writing it into the SD card. 
11-	Insert the microSD card with the Debian 8.10 image into the powered down BBB’s microSD slot. 
12-	Press and hold down the USER/BOOT button and apply power (either by USB cable or 5V adapter).
13-	The 4 LED lights on the BBB will start flashing.
14-	When the flashing is complete, all 4 USRx LEDs will be steady on or off. The latest Debian flasher images automatically power down the board upon completion. This can take up to 45 minutes. 
15-	Once the BBB is powered down after flashing, remove the SD card with the image and apply power again to the board. 
16-	Connect it to a monitor, mouse and keyboard. 
17-	Once powered on, open QTerminal and check the Debian and kernel version (following steps 3-4). They should be as desired. The BBB is now up-to-date. 
18-	On QTerminal, login as root by first typing sudo su and entering password as ‘temppwd’ when asked for password. 
19-	Follow the steps (https://learn.adafruit.com/max31855-thermocouple-python-library/software) to set up the MAX31855 library in order to use sensors for thermal cycling:
a.	Type sudo apt-get update
b.	Type sudo apt-get install build-essential python-dev python-pip python-smbus git
i.	If the command prompts any additional use of space on the BBB, press ‘y’ and then Enter.
c.	Type sudo pip install Adafruit_BBIO
d.	Type cd ~
e.	Type git clone https://github.com/adafruit/Adafruit_Python_MAX31855.git
f.	Type cd Adafruit_Python_MAX31855
g.	Type sudo python setup.py install
h.	The BBB is now ready to go!
20-	Navigate to Desktop on QTerminal by typing in cd /home/Debian/Desktop
21-	Clone the PCR git repository from BitBucket. 
22-	This should set up a folder on the Desktop of BBB. 
23-	The thermal cycler is not ready to be used as per the Thermal Cycler Operations Manual. 


