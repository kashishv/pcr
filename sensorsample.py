#import Adafruit_Python_MAX31855
#import PiPID
#import usr/local/lib/python2.7/dist-packages
#Temp1 = 95
#Temp2 = 55
#!/usr/bin/python
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER

#import logging
#logging.basicConfig(level=logging.DEBUG)

import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_MAX31855.MAX31855 as MAX31855
#import Adafruit_GPIO as GPIO
import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM

def c_to_f(c):
	return c*9.0/5.0 + 32.0

CLK = 'P8_7'
CS = 'P8_9'
DO = 'P8_11'

CLK_1 = 'P9_13'
CS_1 = 'P9_27'
DO_1 = 'P9_30'

CLK_2='P9_12'
CS_2='P9_15'
DO_2 = 'P9_23'


myPWM = "P8_13"
sensor = MAX31855.MAX31855(CLK, CS, DO)
sensor_1 = MAX31855.MAX31855(CLK_1,CS_1,DO_1)
sensor_2 = MAX31855.MAX31855(CLK_2,CS_2,DO_2)


# software or hardware SPI.ITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# BeagleBone Black hardware SPI configuration.
#SPI_PORT   = 1software SPI configuration.
#SPI_DEVICE = 0bug output by uncommenting:
#sensor = MAX31855.MAX31855(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))
#DO  = 18asicConfig(level=logging.DEBUG)
# Loop printing measurements every second.
print('Press Ctrl-C to quit.')
#DC = input("Enter DC ")
while True:
#	DC = input("What duty cycle would you like? ")
#	PWM.start(myPWM, DC, 10000)
#	time.sleep(5)
	temp1  =sensor.readTempC()
	print "Surface :", temp1

	temp2=sensor_1.readTempC()
	print "Heat Sink :",temp2

	temp3=sensor_2.readTempC()
	print "Channel :",temp3
	time.sleep(2)
	print " "
#	PWM.stop(myPWM)
#	PWM.cleanup()

#    temp = sensor.readTempC()
#    internal = sensor.readInternalC()
#    print('Thermocouple Temperature: {0:0.3F}*C / {1:0.3F}*F'.format(temp, c_to_f(temp)))
#    print('       Internal Temperature: {0:0.3F}*C / {1:0.3F}*F'.format(internal, c_to_f(internal)))
#    print (' ')
#    time.sleep(1.0)



